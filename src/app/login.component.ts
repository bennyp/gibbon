import { Component } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'gbn-login',
  template: `
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="error-template">
            <h1>Welcome to <b>Gibbon</b></h1>
            <h2>Group Level GitLab Issue Board</h2>
            <br/>
            <div class="error-actions">
              <a class="btn btn-success btn-lg" (click)="login()">
                <i class="fa fa-user" style="margin-right: 5px;"></i> Log in with GitLab
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
})
export class LoginComponent {

  constructor(private oauthService: OAuthService) {}

  login(): void {
    console.log(this.oauthService);
    this.oauthService.initImplicitFlow();
  }

}
