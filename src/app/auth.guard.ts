import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private oAuthService: OAuthService, private router: Router) {}

  canActivate(): boolean {

    if (this.oAuthService.hasValidAccessToken()) {
        return true;
    } else {
        this.router.navigate(['login']);
        return false;
    }

  }
}
