import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../gitlab/model/user';

@Component({
  selector: 'gbn-navbar',
  templateUrl: './navbar.component.html',
})
export class NavbarComponent implements OnInit {

  user: User;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
      this.user = this.route.snapshot.data['user'];
  }

}
