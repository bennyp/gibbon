import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { GitlabService } from '../gitlab.service';
import { Group } from '../model/group';
import { environment } from '../../../environments/environment';

@Injectable()
export class GroupResolve implements Resolve<Group> {

  constructor(private service: GitlabService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.service.getGroup(environment.gitlabGroup);
  }

}
