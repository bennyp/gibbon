import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { GitlabService } from './gitlab.service';
import { UserResolve } from './resolve/user-resolve.service';
import { GroupResolve } from './resolve/group-resolve.service';
import {IssuesResolve} from './resolve/issues-resolve';

@NgModule({
  imports: [
    CommonModule,
    HttpModule
  ],
  declarations: [],
  providers: [
    GitlabService,
    UserResolve,
    GroupResolve,
    IssuesResolve
  ]
})
export class GitlabModule { }
