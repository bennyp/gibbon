export class Assignee {
  constructor(
    public id: number,
    public name: string,
    public username: string,
    public web_url: string,
    public avatar_url: string,
  ) {}
}
