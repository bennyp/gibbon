import {Project} from './project';

export class Group {
  constructor(
    public id: number,
    public name: number,
    public full_name: string,
    public avatar_url: string,
    public web_url: string,
    public projects: Project[]
  ) {}
}
