import { Assignee } from './assignee';
import { Milestone } from './milestone';

export class Issue {

  constructor(
    public id: number,
    public iid: number,
    public project_id: number,
    public title: string,
    public description: string,
    public state: string,
    public milestone: Milestone,
    public assignee: Assignee,
    public labels: string[],
    public web_url: string,

    //  enriched
    public projectPath: string
  ) {}

}
